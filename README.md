Ansible role: minikube-dns-forwarder
=========

![pipeline](https://gitlab.com/tcblome/ansible-role-minikube-dns-forwarder/badges/master/build.svg)

This role configures dnsmasq to resolve a kubernetes ingress controller domain as the minikube ip


Requirements
------------
minikube
dnsmasq

Installation
------------

Using `ansible-galaxy`:

```shell
$ ansible-galaxy install https://gitlab.com/tcblome/ansible-role-minikube-dns-forwarder.git
```

Using `requirements.yml`:
```yaml
 src: git+https://gitlab.com/tcblome/ansible-role-minikube-dns-forwarder.git
 name: tcblome.minikube-dns-forwarder
```
Using `git`:
```shell
$ git clone https://gitlab.com/tcblome/ansible-role-minikube-dns-forwarder.git tcblome.minikube-dns-forwarder
```

Role Variables
--------------
```yaml

# vars file for ansible-role-minikube-dns-forwarder

# minikube_domain:          minikube.test         # the base domain as specified in kubernetes ingress controller

```

Dependencies
------------

This role can be used independently.

Example Playbook
----------------

Sample :
```
    - hosts: servers
      roles:
        - { role: tcblome.minikube-dns-forwarder,
	         minikube_domain: "minikube.test"
	        }
```

License
------------------
[LICENSE](LICENSE)

Author Information
------------------

This role is published for private use by @tcblome
